# Kaggle's Facebook Recruiting IV: Human or Robot?

Code used [in this Kaggle competition](https://www.kaggle.com/c/facebook-recruiting-iv-human-or-bot). If you are looking at this repository on [Bitbucket](https://bitbucket.org/rinze/kaggle-fr4), this the complete code, along with the wiki and the issues I could have created. The [github one](https://github.com/rinze/kaggle-public/fr4) only contains the code for the submission with the highest score.
